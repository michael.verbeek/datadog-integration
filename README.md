# datadog-integration

Terraform module to setup integration between DataDog and AWS.

This module is able to:
* setup the integration between DD and AWs
* setup a log forwarding stack: https://docs.datadoghq.com/integrations/amazon_web_services/?tab=allpermissions#log-collection
* setup an RDS Enhanced OS metrics forwarding stack: https://docs.datadoghq.com/integrations/amazon_rds/#enhanced-rds-integration

## Terraform versions

The latest terraform version is 0.12 and that is the one supported in the mainline of this module.
For terraform 0.11 see branch `master-0.11` in this same repo.

## Log forwarder

The log forwarder is an embedded CloudFormation stack. To use this, make sure you have the required DataDog API secret pre-created. This is a change from the original. Another is that we substitute the implicit role created by SAM for an explicitly created one.

## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.12 |

## Providers

| Name | Version |
|------|---------|
| aws | n/a |
| datadog | n/a |
| template | n/a |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| account\_specific\_namespace\_rules | Enable/disable specific AWS services from being monitored. Defaults to all enabled | `map(string)` | <pre>{<br>  "api_gateway": "true",<br>  "application_elb": "true",<br>  "appstream": "true",<br>  "appsync": "true",<br>  "athena": "true",<br>  "auto_scaling": "true",<br>  "billing": "true",<br>  "budgeting": "true",<br>  "cloudfront": "true",<br>  "cloudsearch": "true",<br>  "cloudwatch_events": "true",<br>  "cloudwatch_logs": "true",<br>  "codebuild": "true",<br>  "cognito": "true",<br>  "collect_custom_metrics": "true",<br>  "connect": "true",<br>  "crawl_alarms": "true",<br>  "directconnect": "true",<br>  "dms": "true",<br>  "documentdb": "true",<br>  "dynamodb": "true",<br>  "ebs": "true",<br>  "ec2": "true",<br>  "ec2api": "true",<br>  "ec2spot": "true",<br>  "ecs": "true",<br>  "efs": "true",<br>  "elasticache": "true",<br>  "elasticbeanstalk": "true",<br>  "elastictranscoder": "true",<br>  "elb": "true",<br>  "emr": "true",<br>  "es": "true",<br>  "firehose": "true",<br>  "gamelift": "true",<br>  "glue": "true",<br>  "inspector": "true",<br>  "iot": "true",<br>  "kinesis": "true",<br>  "kinesis_analytics": "true",<br>  "kms": "true",<br>  "lambda": "true",<br>  "lex": "true",<br>  "mediaconnect": "true",<br>  "mediaconvert": "true",<br>  "mediapackage": "true",<br>  "mediatailor": "true",<br>  "ml": "true",<br>  "mq": "true",<br>  "msk": "true",<br>  "nat_gateway": "true",<br>  "neptune": "true",<br>  "network_elb": "true",<br>  "opsworks": "true",<br>  "polly": "true",<br>  "rds": "true",<br>  "redshift": "true",<br>  "rekognition": "true",<br>  "route53": "true",<br>  "s3": "true",<br>  "sagemaker": "true",<br>  "ses": "true",<br>  "shield": "true",<br>  "sns": "true",<br>  "sqs": "true",<br>  "step_functions": "true",<br>  "storage_gateway": "true",<br>  "swf": "true",<br>  "translate": "true",<br>  "trusted_advisor": "true",<br>  "vpn": "true",<br>  "waf": "true",<br>  "workspaces": "true",<br>  "xray": "true"<br>}</pre> | no |
| aws\_account\_id | n/a | `any` | n/a | yes |
| cloudtrail\_bucket\_arn | CloudTrail bucket to read logs from | `string` | `""` | no |
| cloudtrail\_kms\_key\_arn | CloudTrail KMS key to decrypt logs | `string` | `""` | no |
| datadog\_api\_key\_secret\_arn | Secret ARN for DataDog API Key | `string` | `""` | no |
| datadog\_forwarder\_stack\_template | Location of the DataDog stack template | `string` | `""` | no |
| datadog\_forwarder\_version | Which forwarder version you should use | `string` | `"3.6.0"` | no |
| datadog\_include\_at\_match | Regex pattern on which to filter. if set to empty string, no filtering is applied | `string` | `""` | no |
| enable | Whether to enable this module | `bool` | `true` | no |
| enable\_datadog\_forwarder\_lambda | Whether to enable the DD forwarder lambda stack | `bool` | `false` | no |
| enable\_guardduty\_cloudwatch\_event | Whether to enable cloudwatch events for GuardDuty | `bool` | `false` | no |
| filter\_tags | Tags used to filter which resources should be monitored by datadog. Set to empty map to monitor all resources. | `map(string)` | <pre>{<br>  "com.datadoghq.app": "enabled"<br>}</pre> | no |
| name\_prefix | Name prefix for all resources that will take them | `string` | `""` | no |
| tags | Tags appended to all resources that will take them | `map(string)` | `{}` | no |
| use\_default\_datadog\_forwarder\_stack\_template | Whether to use the stack template included in the module | `bool` | `true` | no |

## Outputs

| Name | Description |
|------|-------------|
| datadog\_integration\_role\_arn | n/a |
| datadog\_integration\_role\_name | n/a |
| datadog\_log\_forwarder\_role\_arn | n/a |
| datadog\_log\_forwarder\_role\_name | n/a |
| datadog\_log\_forwarder\_stack\_id | n/a |
| datadog\_log\_forwarder\_stack\_outputs | n/a |
| filter\_tags | n/a |
