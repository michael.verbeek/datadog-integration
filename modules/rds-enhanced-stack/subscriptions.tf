module "subscription" {
  source = "../rds-enhanced-cloudwatch-log-subscription"

  enabled = var.enabled && var.configure_rds_subscription

  name_prefix = var.name_prefix
  tags        = var.tags

  datadog_rds_function_arn  = local.function_arn
  datadog_rds_function_name = local.function_name
  datadog_rds_iam_role_arn  = local.role_arn
  datadog_rds_iam_role_name = local.role_name

  cloudwatch_log_group_name              = var.cloudwatch_log_group_name
  cloudwatch_subscription_filter_pattern = var.cloudwatch_subscription_filter_pattern
  cloudwatch_subscription_distribution   = var.cloudwatch_subscription_distribution

  subscriptions = ["rds-enhanced"]
}
