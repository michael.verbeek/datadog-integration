locals {
  kms_key_id = module.rds_cmk.key_id

  default_kms_policy = [{
    # Allow lambda execution role to use the key for decrypting DD api key
    principals = [{
      type = "AWS", identifiers = [local.role_arn]
    }]

    effect    = "Allow"
    actions   = ["kms:Decrypt"]
    resources = ["*"]
    condition = []
    # Allow us (the identity behind the provider) to encypt the api keys for the stack
    },
    {
      principals = [{
        type = "AWS", identifiers = [join("", data.aws_caller_identity.current.*.arn)]
      }]

      effect    = "Allow"
      actions   = ["kms:Encrypt"]
      resources = ["*"]
      condition = []
  }]

  kms_key_policy = concat(local.default_kms_policy, var.custom_rds_stack_kms_key_policy)
}

module "rds_cmk" {
  source = "git::https://gitlab.com/open-source-devex/terraform-modules/aws/kms-key.git?ref=v1.0.5"

  create_key = var.enabled

  key_name        = "${local.resource_name_prefix}-datadog-rds-stack"
  key_description = "CMK for DD RDS Enhanced Stack ${local.resource_name_prefix}"
  tags            = var.tags

  key_policy = local.kms_key_policy
}

resource "aws_kms_ciphertext" "datadog_api_key_encrypted" {
  count = var.enabled ? 1 : 0

  key_id    = local.kms_key_id
  plaintext = local.lambda_api_key_payload
}

data "aws_caller_identity" "current" {
  count = var.enabled ? 1 : 0
}
