locals {
  role_name = join("", aws_iam_role.rds_role.*.name)
  role_arn  = join("", aws_iam_role.rds_role.*.arn)
}

resource "aws_iam_role" "rds_role" {
  count = var.enabled ? 1 : 0

  name        = "${local.resource_name_prefix}-datadog-rds"
  description = "Allows DataDog RDS Enhanced to access AWS resources"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": {
    "Effect": "Allow",
    "Action": "sts:AssumeRole",
    "Principal": {
      "Service": "lambda.amazonaws.com"
    }
  }
}
EOF

  tags = var.tags
}

resource "aws_iam_role_policy_attachment" "rds_lambda_basic_execution" {
  count = var.enabled ? 1 : 0

  role       = join("", aws_iam_role.rds_role.*.name)
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}
