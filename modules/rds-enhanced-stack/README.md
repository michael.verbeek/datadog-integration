# RDS Enhanced Monitoring Stack

This module setups the DD RDS Enhanced Monitoring Stack.
For this stack a lambda is used and that usually is obtained form a DD S3 bucket.
however, the key for the object keeps changing and that breaks the module.
To that end, the module expects a S3 bucket and a S3 object as input.
The module contains a AWS SAM template adapted form the original.

The source for both the SAM template and the lambda function code is this repo https://github.com/DataDog/datadog-serverless-functions/tree/master/aws/rds_enhanced_monitoring.
Please upload the lambda zip to a S3 bucket and wire those parameters in the module.
