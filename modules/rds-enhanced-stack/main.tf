locals {
  resource_name_prefix = var.name_prefix != "" ? var.name_prefix : "dd-integration"

  use_default_stack_template = var.custom_rds_stack_template == null


  function_arn  = lookup(try(aws_cloudformation_stack.rds[0].outputs, {}), "DatadogRdsArn", "")
  function_name = try(reverse(split(":", local.function_arn))[0], "") # Get function name from ARN

  # generate JSON from map, and then escape double quotes
  lambda_api_key_payload = jsonencode({
    api_key = var.datadog_api_key
  })

  stack_template = var.enabled && local.use_default_stack_template ? templatefile("${path.module}/files/cloudformation-stack-template.yaml", {
    lambda_role_arn = local.role_arn
    encrypted_keys  = join("", aws_kms_ciphertext.datadog_api_key_encrypted.*.ciphertext_blob)
    datadog_site    = var.datadog_site
    python_version  = var.custom_rds_stack_python_version
    code_s3_bucket  = var.code_s3_bucket
    code_s3_object  = var.code_s3_object
  }) : ""
}

resource "aws_cloudformation_stack" "rds" {
  count = var.enabled ? 1 : 0

  name = "${local.resource_name_prefix}-datadog-rds-stack"

  parameters = {
    KMSKeyId = local.kms_key_id
  }

  capabilities = ["CAPABILITY_IAM", "CAPABILITY_NAMED_IAM", "CAPABILITY_AUTO_EXPAND"]

  template_body = local.use_default_stack_template ? local.stack_template : var.custom_rds_stack_template

  tags = var.tags
}
