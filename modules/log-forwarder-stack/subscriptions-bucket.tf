locals {
  bucket_subscriptions         = var.enabled ? var.forwarder_bucket_subscriptions : {}
  bucket_subscription_kms_keys = { for name, info in local.bucket_subscriptions : name => info.kms_key_arn if info.is_encrypted }
}

module "bucket_subscriptions" {
  source = "../log-forwarder-bucket-subscription"

  enabled     = var.enabled
  name_prefix = var.name_prefix
  tags        = var.tags

  datadog_forwarder_function_arn  = local.function_arn
  datadog_forwarder_function_name = local.function_name
  datadog_forwarder_iam_role_name = local.role_name

  subscriptions = local.bucket_subscriptions
}
