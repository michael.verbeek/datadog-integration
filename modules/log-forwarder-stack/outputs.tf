output "log_forwarder_stack_id" {
  value = join("", aws_cloudformation_stack.forwarder.*.id)
}

output "log_forwarder_stack_outputs" {
  value = try(aws_cloudformation_stack.forwarder[0].outputs, {})
}

output "log_forwarder_iam_role_arn" {
  value = local.role_arn
}

output "log_forwarder_iam_role_name" {
  value = local.role_name
}

output "log_forwarder_function_arn" {
  value = local.function_arn
}

output "log_forwarder_function_name" {
  value = local.function_name
}

output "api_key_secret_name" {
  value = local.datadog_credentials_secret_name
}
