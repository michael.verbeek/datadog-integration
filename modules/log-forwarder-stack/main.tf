locals {
  resource_name_prefix = var.name_prefix != "" ? var.name_prefix : "dd-integration"

  use_default_stack_template = var.custom_forwarder_stack_template == null

  function_arn  = lookup(try(aws_cloudformation_stack.forwarder[0].outputs, {}), "DatadogForwarderArn", "")
  function_name = try(reverse(split(":", local.function_arn))[0], "") # Get function name from ARN

  role_arn  = join("", aws_iam_role.forwarder_role.*.arn)
  role_name = join("", aws_iam_role.forwarder_role.*.name)

  stack_template = var.enabled && local.use_default_stack_template ? templatefile("${path.module}/files/cloudformation-stack-template.yaml", {
    datadog_forwarder_version = var.forwarder_version
    lambda_role_arn           = join("", aws_iam_role.forwarder_role.*.arn)
  }) : ""
}

# Lambda IAM
resource "aws_iam_role" "forwarder_role" {
  count = var.enabled ? 1 : 0

  name        = "${local.resource_name_prefix}-datadog-log-forwarder"
  description = "Allows DataDog Forwarder to access AWS resources"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": {
    "Effect": "Allow",
    "Action": "sts:AssumeRole",
    "Principal": {
      "Service": "lambda.amazonaws.com"
    }
  }
}
EOF

  tags = var.tags
}


data "aws_iam_policy_document" "forwarder_lambda" {
  count = var.enabled ? 1 : 0

  statement {
    sid = "AllowFetchS3BucketObjects"

    actions = [
      "s3:GetObject"
    ]

    resources = ["arn:aws:s3:::*"]
  }

  statement {
    sid = "AllowFetchDataDogAPISecret"

    actions = [
      "secretsmanager:GetSecretValue"
    ]

    resources = [local.datadog_api_key_secret_arn]
  }
}


resource "aws_iam_policy" "forwarder_lambda" {
  count = var.enabled ? 1 : 0

  name   = "${local.resource_name_prefix}-datadog-log-forwarder-lambda-policy"
  policy = join("", data.aws_iam_policy_document.forwarder_lambda.*.json)
}

resource "aws_iam_role_policy_attachment" "forwarder_lambda_generic_execution" {
  count = var.enabled ? 1 : 0

  role       = join("", aws_iam_role.forwarder_role.*.name)
  policy_arn = join("", aws_iam_policy.forwarder_lambda.*.arn)
}

resource "aws_iam_role_policy_attachment" "forwarder_lambda_basic_execution" {
  count = var.enabled ? 1 : 0

  role       = join("", aws_iam_role.forwarder_role.*.name)
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

resource "aws_cloudformation_stack" "forwarder" {
  count = var.enabled ? 1 : 0

  name = "${local.resource_name_prefix}-datadog-forwarder-stack"

  parameters = {
    DdSite         = var.datadog_site
    DdApiKeySecret = local.datadog_api_key_secret_arn
    IncludeAtMatch = var.forwarder_include_at_match
  }
  capabilities = ["CAPABILITY_IAM", "CAPABILITY_NAMED_IAM", "CAPABILITY_AUTO_EXPAND"]

  template_body = local.use_default_stack_template ? local.stack_template : var.custom_forwarder_stack_template

  tags = var.tags
}
