variable "name_prefix" {
  description = "Name prefix for all resources that will take them"
  type        = string
  default     = ""
}

variable "tags" {
  description = "Tags appended to all resources that will take them"
  type        = map(string)
  default     = {}
}

variable "enabled" {
  description = "Whether to create the resources in this module"
  type        = bool
  default     = true
}

variable "forwarder_version" {
  description = "Which forwarder version you should use"
  type        = string
  default     = "3.6.0"
}

variable "datadog_site" {
  description = "The DD site to set as a parameter to the cloudFormation stack"
  type        = string
  default     = "datadoghq.eu"
}

variable "datadog_api_key" {
  description = "The API key used for the DataDog provider"
  type        = string
  default     = ""
}

variable "forwarder_include_at_match" {
  description = "Regex pattern on which to filter. if set to empty string, no filtering is applied"
  type        = string
  default     = ""
}

variable "custom_forwarder_stack_template" {
  description = "A stack template to be used instead of the default DD stack template"
  type        = string
  default     = null
}

variable "create_forwarder_api_key_secret" {
  description = "Whther to create a AWS SecretsManager secret for DD API key, when set to false `datadog_api_key_secret_arn` must be set to a valid ARN"
  type        = bool
  default     = true
}

variable "forwarder_api_key_secret_arn" {
  type        = string
  description = "The ARN of a AWS SecretsManager secret containing the DD API key to be used in the forwarding stack"
  default     = ""
}

variable "forwarder_bucket_subscriptions" {
  description = "Map of bucket names to a pairs of bucket ARN and KMS key ARN. This map is used to configure forwarding logs from the respective bucekts to DD."

  type = map(object({
    bucket_arn   = string
    is_encrypted = bool
    kms_key_arn  = string
  }))

  default = {
    # bucket-name = {
    #   bucket_arn  = "arn"
    #   kms_key_arn = "arn"
    # }
  }
}


variable "forwarder_cloudwatch_event_subscriptions" {
  description = "Map of CloudWatch event subscriptions"

  type = map(object({
    source      = string
    detail_type = string
  }))

  default = {
    # guardduty-finding = {
    #   source = "aws.guardduty"
    #   detail_type = "GuardDuty Finding"
    #
  }
}
