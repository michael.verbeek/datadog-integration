variable "enabled" {
  description = "Whether to create the resources in this module"
  type        = bool
  default     = true
}

variable "name_prefix" {
  description = "Name prefix for all resources that will take them"
  type        = string
  default     = ""
}

variable "tags" {
  description = "Tags appended to all resources that will take them"
  type        = map(string)
  default     = {}
}

variable "subscriptions" {
  description = "Map of bucket names to a pairs of bucket ARN and KMS key ARN. This map is used to configure forwarding logs from the respective bucekts to DD."

  type = set(string)

  default = []
}

variable "datadog_rds_function_arn" {
  description = "The ARN of the DD forwarder stack lambda"
  type        = string
}

variable "datadog_rds_function_name" {
  description = "The name of the DD forwarder stack lambda"
  type        = string
}

variable "datadog_rds_iam_role_arn" {
  description = "The ARN of the IAM role for DataDog"
  type        = string
}

variable "datadog_rds_iam_role_name" {
  description = "The name of the IAM role for DataDog"
  type        = string
}

variable "cloudwatch_log_group_name" {
  description = "The name of the CloudWatch log group for RDS OS metrics"
  type        = string
  default     = "RDSOSMetrics"
}

variable "cloudwatch_subscription_filter_pattern" {
  description = "A valid CloudWatch Logs filter pattern for subscribing to a filtered stream of log events."
  type        = string
  default     = ""
}

variable "cloudwatch_subscription_distribution" {
  description = "The method used to distribute log data to the destination. By default log data is grouped by log stream, but the grouping can be set to random for a more even distribution. This property is only applicable when the destination is an Amazon Kinesis stream. Valid values are \"Random\" and \"ByLogStream\"."
  type        = string
  default     = "Random"
}
