locals {
  subscriptions = var.enabled ? var.subscriptions : []
}

##########################################################################################################
# Implementation of CloudWatch log destination triggering lambda function
# https://docs.aws.amazon.com/AmazonCloudWatch/latest/logs/SubscriptionFilters.html#LambdaFunctionExample
##########################################################################################################
resource "aws_cloudwatch_log_subscription_filter" "log_subscription" {
  for_each = local.subscriptions

  name            = "${var.name_prefix}-${each.key}"
  log_group_name  = var.cloudwatch_log_group_name
  filter_pattern  = var.cloudwatch_subscription_filter_pattern
  destination_arn = var.datadog_rds_function_arn
  distribution    = var.cloudwatch_subscription_distribution

  depends_on = [aws_lambda_permission.log_subscription]
}

#############################
# Forwarder Stack Lambda
#############################
resource "aws_lambda_permission" "log_subscription" {
  for_each = local.subscriptions

  statement_id  = "AllowExecutionFromCloudWatch-${each.key}"
  action        = "lambda:InvokeFunction"
  function_name = var.datadog_rds_function_name
  principal     = "logs.amazonaws.com"
  source_arn    = join("", data.aws_cloudwatch_log_group.rds_os_metrics.*.arn)
}

data "aws_cloudwatch_log_group" "rds_os_metrics" {
  count = var.enabled && length(local.subscriptions) > 0 ? 1 : 0

  name = var.cloudwatch_log_group_name
}
