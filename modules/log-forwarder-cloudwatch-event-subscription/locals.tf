locals {
  resource_name_prefix = var.name_prefix != "" ? var.name_prefix : "dd-integration"
}
