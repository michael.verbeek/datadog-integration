provider "aws" {
  region = "eu-west-1"
}

variable "datadog_api_key" {}
variable "datadog_app_key" {}


provider "datadog" {
  api_key = var.datadog_api_key
  app_key = var.datadog_app_key
  api_url = "https://api.datadoghq.eu/"
}

module "datadog_integration" {
  source = "../../"

  datadog_api_key = var.datadog_api_key

  enable_datadog_forwarder_stack = true
  enable_datadog_rds_stack       = true

  filter_tags = {}

  tags = {
    dd-integration-test = "dd-integration-test"
  }
}
