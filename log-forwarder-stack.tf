module "log_forwarder_stack" {
  source = "./modules/log-forwarder-stack"

  name_prefix = var.name_prefix
  tags        = var.tags
  enabled     = var.enabled && var.enable_datadog_forwarder_stack

  datadog_site    = var.datadog_site
  datadog_api_key = var.datadog_api_key

  create_forwarder_api_key_secret = var.create_forwarder_api_key_secret
  forwarder_api_key_secret_arn    = var.datadog_forwarder_api_key_secret_arn
  forwarder_version               = var.datadog_forwarder_version

  custom_forwarder_stack_template = var.custom_datadog_forwarder_stack_template
  forwarder_include_at_match      = var.datadog_forwarder_include_at_match

  forwarder_bucket_subscriptions           = var.datadog_forwarder_bucket_subscriptions
  forwarder_cloudwatch_event_subscriptions = var.datadog_forwarder_cloudwatch_event_subscriptions
}
