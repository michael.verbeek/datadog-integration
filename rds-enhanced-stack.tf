module "rds_enhanced_stack" {
  source = "./modules/rds-enhanced-stack"

  name_prefix = var.name_prefix
  tags        = var.tags
  enabled     = var.enabled && var.enable_datadog_rds_stack

  datadog_api_key = var.datadog_api_key

  configure_rds_subscription             = var.datadog_rds_configure_subscription
  custom_rds_stack_template              = var.custom_datadog_rds_stack_template
  cloudwatch_log_group_name              = var.datadog_rds_cloudwatch_log_group_name
  cloudwatch_subscription_filter_pattern = var.datadog_rds_cloudwatch_subscription_filter_pattern
  cloudwatch_subscription_distribution   = var.datadog_rds_cloudwatch_subscription_distribution
  datadog_site                           = var.datadog_site
  custom_rds_stack_python_version        = var.custom_rds_stack_python_version
  custom_rds_stack_kms_key_policy        = var.custom_rds_stack_kms_key_policy


  code_s3_bucket = var.datadog_rds_code_s3_bucket
  code_s3_object = var.datadog_rds_code_s3_object
}
