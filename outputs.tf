output "filter_tags" {
  value = var.filter_tags
}

output "datadog_integration_role_arn" {
  value = join("", aws_iam_role.datadog_integration.*.arn)
}

output "datadog_integration_role_name" {
  value = join("", aws_iam_role.datadog_integration.*.name)
}

output "datadog_log_forwarder_stack_id" {
  value = module.log_forwarder_stack.log_forwarder_stack_id
}

output "datadog_log_forwarder_stack_outputs" {
  value = module.log_forwarder_stack.log_forwarder_stack_outputs
}

output "datadog_log_forwarder_iam_role_arn" {
  value = module.log_forwarder_stack.log_forwarder_iam_role_arn
}

output "datadog_log_forwarder_iam_role_name" {
  value = module.log_forwarder_stack.log_forwarder_iam_role_name
}

output "datadog_log_forwarder_function_arn" {
  value = module.log_forwarder_stack.log_forwarder_function_arn
}

output "datadog_log_forwarder_function_name" {
  value = module.log_forwarder_stack.log_forwarder_function_name
}

output "datadog_log_forwarder_api_key_secret_name" {
  value = module.log_forwarder_stack.api_key_secret_name
}

output "datadog_rds_enhanced_stack_id" {
  value = module.rds_enhanced_stack.rds_enhanced_stack_id
}

output "datadog_rds_enhanced_stack_outputs" {
  value = module.rds_enhanced_stack.rds_enhanced_stack_outputs
}

output "datadog_rds_enhanced_iam_role_arn" {
  value = module.rds_enhanced_stack.rds_enhanced_iam_role_arn
}

output "datadog_rds_enhanced_iam_role_name" {
  value = module.rds_enhanced_stack.rds_enhanced_iam_role_name
}

output "datadog_rds_enhanced_function_arn" {
  value = module.rds_enhanced_stack.rds_enhanced_function_arn
}

output "datadog_rds_enhanced_function_name" {
  value = module.rds_enhanced_stack.rds_enhanced_function_name
}
