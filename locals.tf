locals {
  resource_name_prefix = var.name_prefix != "" ? var.name_prefix : "dd-integration"

  enable_datadog_forwarder_stack = var.enabled && var.enable_datadog_forwarder_stack

  aws_account_id = var.aws_account_id != null ? var.aws_account_id : join("", data.aws_caller_identity.current.*.account_id)
}

data "aws_caller_identity" "current" {
  count = var.enabled ? 1 : 0
}
